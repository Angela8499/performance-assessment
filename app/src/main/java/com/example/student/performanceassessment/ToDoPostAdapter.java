package com.example.student.performanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoPostAdapter extends RecyclerView.Adapter<ToDoPostHolder>{
    private ArrayList<TodoPost> todoItems;

    public ToDoPostAdapter(ActivityCallback activityCallback, ArrayList<TodoPost> todoItems ){
        this.todoItems = todoItems;
    }

    @Override
    public ToDoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoPostHolder holder, int position) {
        holder.titleText.setText(todoItems.get(position).title);
    }

    @Override
    public int getItemCount() {
        return todoItems.size();
    }
}